import uuid
import logging
import hashlib
import settings
import simpledb

from utils import slugify
from simpledb import models
from datetime import datetime


from boto.s3.connection import S3Connection
from boto.s3.key import Key

from StringIO import StringIO

from boto.sqs.connection import SQSConnection
from boto.sqs.message import Message

AW_SDB = simpledb.SimpleDB(
    settings.AWS_KEY,
    settings.AWS_SECRET
)


class SystemUser(models.Model):

    __id = models.ItemName()
    username = models.Field(required=True)
    password = models.Field(required=True)
    email = models.Field(required=True)
    status = models.Field(default='enabled')
    created_at = models.DateTimeField(default=datetime.now())
    modified_at = models.DateTimeField()
    last_login_at = models.DateTimeField()

    class Meta:
        connection = AW_SDB
        domain = 'system_user'

    @classmethod
    def email_exists(self, email):
        rs = self.objects.filter(
            simpledb.item_name(eq=email)
        )
        return True if len(rs) > 0 else False

    def set_password(self, value):
        if isinstance(value, unicode):
            value = value.encode('utf8')
        self.password = hashlib.sha1(value).hexdigest()

    @classmethod
    def auth(self, email, password):
        password = str(hashlib.sha224(password).hexdigest())
        tot = len(self.objects.filter(
            simpledb.where(email=email) & \
            simpledb.where(password=password) & \
            simpledb.where(status='active')
        ))
        return True if tot > 0 else False

    def save(self, **kwargs):
        if self.password:
            self.password = hashlib.sha224(self.password).hexdigest()
        super(User, self).save(**kwargs)


class User(models.Model):
    email = models.ItemName()
    name = models.Field(required=True)
    lastname = models.Field(required=True)
    gender = models.Field(required=True)
    password = models.Field(default='')
    status = models.Field(default='inactive')
    score = models.NumberField(default=0)
    created_at = models.DateTimeField(default=datetime.now())
    modified_at = models.DateTimeField(default=datetime.now())
    last_login_at = models.DateTimeField()
    activation = models.Field(default='')
    activation_passwd = models.Field(default='')
    activation_passwd_facebook = models.Field(default='')
    facebook_id = models.Field(default='')

    class Meta:
        connection = AW_SDB
        domain = 'user'

    #@classmethod
    def set_password(self):
        if self.password:
            self.password = hashlib.sha224(self.password).hexdigest()

    @classmethod
    def email_exists(self, email):
        rs = self.objects.filter(
            simpledb.item_name(eq=email)
        )
        return True if len(rs) > 0 else False

    @classmethod
    def auth(self, email, password):
        password = str(hashlib.sha224(password).hexdigest())
        tot = len(self.objects.filter(
            simpledb.item_name(eq=email) & \
            simpledb.where(password=password) & \
            simpledb.where(status='active')
        ))
        return True if tot > 0 else False

    def set_password(self, password):
        self.password = hashlib.sha224(password).hexdigest()

    def save(self, **kwargs):
        if self.password:
            self.password = hashlib.sha224(self.password).hexdigest()
        super(User, self).save(**kwargs)

    def update(self, **kwargs):
        if self.status == 'active':
            self.activation = ''
        self.modified_at = datetime.now()
        super(User, self).save(**kwargs)

    @classmethod
    def get_token(self, field):
        while True:
            activation = ('%s%s' % (
                settings.COOKIE_SECRET,
                uuid.uuid4().hex))[::-1]
            try:
                if self.objects.filter(field=activation).count() == 0:
                    activation = activation
                    break
            except Exception as exc:
                logging.error(exc)
                break
        return activation

    @property
    def comments(self):
        return Comment.objects.filter(
            simpledb.every(user=self.id)
        )

    @property
    def matches(self):
        return Match.objects.filter(
            simpledb.every(user=self.id)
        )

    @property
    def points(self):
        try:
            return Points.objects.get(self.email)
        except Exception as exc:
            logging.error(exc)
            return None


class Category(models.Model):
    slug = models.ItemName()
    title = models.Field(default='')
    status = models.Field(default='disabled')
    created_at = models.DateTimeField(default=datetime.now())
    last_modified_at = models.DateTimeField(default='')
    total_products = models.NumberField(default=0)
    total_products_active = models.NumberField(default=0)
    total_products_inactive = models.NumberField(default=0)
    total_products_have = models.NumberField(default=0)
    total_products_want = models.NumberField(default=0)

    class Meta:
        connection = AW_SDB
        domain = 'category'

    @classmethod
    def _generate_slug(cls, title):
        _count = 2
        _slug = slugify(title)
        while len(cls.objects.filter(slug=_slug)) > 0:
            _slug = slugify(u'%s %s' % (title, _count))
            _count += 1
        return _slug

    def update(self, **kwargs):
        super(Category, self).save(**kwargs)

    def save(self, **kwargs):
        self.slug = self._generate_slug(self.title)
        super(Category, self).save(**kwargs)

    def delete(self):
        for product in self.products:
            index = product.categories.index(self.id)
            del product.categories[index]
            product.update()
        super(Category, self).delete()

    @property
    def products(self):
        return Product.objects.filter(
            categories=self.id
        )


class Product(models.Model):

    THUMB_SIZES_IMAGES = (
        (640, 428, 'large'),
        (377, 286, 'medium'),
        (165, 132, 'small'),
        (88, 69, 'tiny'),
    )

    AUDIENCES_TYPES = (('all', 'all'),
                        ('local', 'local'),
                        ('private group', 'private group'))

    __id = models.ItemName()
    slug = models.Field(default='')
    title = models.Field(default='')
    description = models.Field(default='')
    cost = models.NumberField(default=0)
    will_travel = models.NumberField(default=0)
    allow_retailers = models.BooleanField(default=False)
    allow_charities = models.BooleanField(default=False)
    donate_now = models.BooleanField(default=False)
    status = models.Field(required=True, default='inactive')
    created_at = models.DateTimeField(default=datetime.now())
    last_modified_at = models.DateTimeField()
    published_at = models.DateTimeField()
    #expires_at = models.DateTimeField()
    expires_at = models.Field()
    type = models.Field(default='')
    user = models.Field()
    categories = models.Field()
    #image = models.Field()
    location = models.Field()
    audience = models.Field()
    is_upload_thumbs = models.BooleanField(default=False)

    class Meta:
        connection = AW_SDB
        domain = 'product'

    @property
    def image(self):
        return '%s%s' % (settings.S3_UPLOAD_URL_PRODUCT, self.id)

    @property
    def tiny_url(self):
        return '%s_%s' % (self.image, 'tiny')

    @property
    def small_url(self):
        return '%s_%s' % (self.image, 'small')

    @property
    def medium_url(self):
        return '%s_%s' % (self.image, 'medium')

    @property
    def large_url(self):
        return '%s_%s' % (self.image, 'large')

    @property
    def id(self):
        return self.__id

    @classmethod
    def _generate_slug(cls, title):
        _count = 2
        _slug = slugify(title)

        while len(cls.objects.filter(slug=_slug)) > 0:
            _slug = slugify(u'%s %s' % (title, _count))
            _count += 1
        return _slug

    def save(self, **kwargs):
        self.__id = str(uuid.uuid4())
        self.slug = self._generate_slug(self.title)
        super(Product, self).save(**kwargs)

    def update(self, **kwargs):
        self.last_modified_at = datetime.now()
        super(Product, self).save(**kwargs)

    def delete(self, **kwargs):

        for conversation in self.conversations:
            logging.info(conversation)
            try:
                conversation.delete()
            except Exception as exc:
                logging.error(exc)

        for match in Match.objects.filter(source=self.id):
            logging.info(match)
            try:
                match.delete()
            except Exception as exc:
                logging.error(exc)
                #match.delete()

        for match in Match.objects.filter(target=self.id):
            logging.info(match)
            try:
                match.delete()
            except Exception as exc:
                logging.error(exc)
                #match.delete()

        cn = S3Connection(
            settings.AWS_KEY, settings.AWS_SECRET
        )

        for suffix in ('', '_large', '_medium', '_small', '_tiny'):

            logging.info('%s/%s%s' % (settings.S3_UPLOAD_PATH_PRODUCT,
                self.id, suffix))

            try:
                key = cn.get_bucket(settings.S3_BUCKET_NAME).get_key(
                    '%s/%s%s' % (settings.S3_UPLOAD_PATH_PRODUCT,
                        self.id, suffix))
                key.delete()
            except Exception as exc:
                logging.error(exc)

        """
        try:
            product.delete()
        except Exception as exc:
            logging.error(exc)
            status_code = 2
        else:
            cn = S3Connection(
                settings.AWS_KEY, settings.AWS_SECRET
            )

            for suffix in ('', '_large', '_medium', '_small', '_tiny'):

                logging.info('%s/%s%s' % (settings.S3_UPLOAD_PATH_PRODUCT,
                    product.id, suffix))

                try:
                    key = cn.get_bucket(settings.S3_BUCKET_NAME).get_key(
                        '%s/%s%s' % (settings.S3_UPLOAD_PATH_PRODUCT,
                            product.id, suffix))
                    key.delete()
                except Exception as exc:
                    logging.error(exc)
        """

        super(Product, self).delete(**kwargs)

    def is_owner(self, user):
        return True if user.email == self.user else False

    @classmethod
    def get_by_slug(self, slug):
        try:
            product = Product.objects.filter(slug=slug)[0]
        except Exception as exc:
            logging.error(exc)
            return None
        else:
            return product

    def get_conversation(self, with_user):
        conversation = None
        try:
            conversation = Conversation.objects.filter(
                product=self.id, with_user=with_user)[0]
        except Exception as exc:
            logging.warn(exc)
        return conversation

    @property
    def data_user(self):
        try:
            return User.objects.get(
                self.user, consistent_read=True)
        except:
            return None

    @property
    def conversations(self):
        return Conversation.objects.filter(product=self.id)

    @property
    def data_categories(self):
        res = []
        if isinstance(self.categories, str):
            try:
                res = [Category.objects.get(self.categories)]
            except:
                pass
        else:
            try:
                res = [Category.objects.get(id) for id in self.categories]
            except:
                pass
        return res

    @property
    def expire_days(self):
        days = 0
        try:
            expires_at = datetime.strptime(
                self.expires_at, '%Y-%m-%d'
            )

            #print self.expires_at
            #print type(self.expires_at)

            days = (expires_at.date() - datetime.now().date()).days
        except Exception as exc:
            logging.error(exc)
        return days

    @property
    def expired(self):
        return False if self.expire_days > 0 else True

    #@classmethod
    """
    def set_slug(self, title):
        if not self.slug or self.slug is None or self.slug is '':
            self.slug = self._generate_slug(title)
    """

    @property
    def matches_want(self):
        data = []
        matches = Match.objects.filter(source=self.id)
        for x in matches:
            product = Product.objects.get(x.target)
            data.append(dict(title=product.title,
                image=product.image, slug=product.slug))
        return data

    @property
    def matches_have(self):
        data = []
        matches = Match.objects.filter(target=self.id)
        for x in matches:
            product = Product.objects.get(x.source)
            data.append(dict(title=product.title,
                image=product.image, slug=product.slug))
        return data

    @property
    def notifications(self):
        data = []
        matches = Match.objects.filter(source=self.id, status='inactive')
        for x in matches:
            product_have = Product.objects.get(x.target)
            data.append(dict(id=x.id, title=product_have.title,
                image=product_have.image,
                user=product_have.data_user.name))
        return data

    def upload_img(self, body):

        try:
            fp = StringIO(body)

            cn = S3Connection(
                settings.AWS_KEY, settings.AWS_SECRET
            )

            bucket = cn.get_bucket(settings.S3_BUCKET_NAME)

            key = Key(bucket)
            key.key = '%s/%s' % (settings.S3_UPLOAD_PATH_PRODUCT, self.id)
            key.set_contents_from_file(
                fp, headers={'Content-Type': 'image/jpeg'}
            )

            key.set_acl('public-read')
            url = 'http://%s.%s/%s' % (
                bucket.name,
                settings.S3_IMAGES_URL,
                key.key
            )
        except Exception as exc:
            logging.error(exc)
            return None
        else:
            return url

    def send_message_to_generate_thumbs(self):
        conn = SQSConnection(settings.AWS_KEY, settings.AWS_SECRET)
        q = conn.create_queue('generate_thumbs')
        m = Message()
        m.set_body(self.id)

        try:
            status = q.write(m)
        except Exception as exc:
            logging.error(exc)
            return None
        else:
            if not status:
                return None
            else:
                return True

    def save_points(self, _status):

        if not Points.email_exists(self.user):
            logging.info('try to save points')

            points = Points()
            points.products = 1
            points.user = self.user

            logging.info('allow_charities: %s' % self.allow_charities)
            if self.allow_charities:
                points.charity = 1

            points.total = int(points.matching) + \
                int(points.charity) + int(points.products)

            try:
                points.save()
            except Exception as exc:
                logging.error(exc)
            else:
                logging.info('save point')

        else:
            logging.info('_status: %s' % _status)

            if _status == 'inactive':

                try:
                    points = Points.objects.get(self.user)
                except Exception as exc:
                    logging.error(exc)
                else:
                    points.products = int(points.products) + 1

                    if self.allow_charities:
                        points.charity = int(points.charity) + 1

                    points.total = int(points.matching) + \
                        int(points.charity) + int(points.products)

                    try:
                        points.update()
                    except Exception as exc:
                        logging.error(exc)
                    else:
                        logging.info('update points')
            else:
                logging.info('cant not')


class Conversation(models.Model):
    __id = models.ItemName()
    product = models.Field(required=True)
    status = models.Field(default='disabled')
    created_at = models.DateTimeField(default=datetime.now())
    last_modified_at = models.DateTimeField()
    unread_comments = models.NumberField(default=0)
    with_user = models.Field(required=True)

    class Meta:
        connection = AW_SDB
        domain = 'conversation'

    @property
    def id(self):
        return self.__id

    @property
    def user_data(self):
        return User.objects.get(self.with_user)

    @property
    def get_conversation(self, id):
        return Comment.objects.filter(
            conversation=self.id
        )

    @property
    def comments(self):
        return Comment.objects.filter(
            conversation=self.id
        )

    @classmethod
    def get(self, id):

        try:
            conversation = self.objects.get(id)
        except Exception as exc:
            logging.error(exc)
            return None
        else:
            return conversation

    def save(self, **kwargs):
        self.__id = str(uuid.uuid4())
        super(Conversation, self).save(**kwargs)

    def update(self, **kwargs):
        self.last_modified_at = datetime.now()
        super(Conversation, self).save(**kwargs)

    def increase_unread_comments(self):
        self.unread_comments = int(
            self.unread_comments) + 1


class Comment(models.Model):
    __id = models.ItemName()
    description = models.Field(required=True)
    conversation = models.Field(required=True)
    status = models.Field(default='disabled')
    created_at = models.DateTimeField(default=datetime.now())
    published_at = models.DateTimeField()
    user = models.Field(required=True)

    @property
    def created_at_f(self):
        try:
            return self.created_at.strftime('%Y-%m-%d %H:%M:%S')
        except:
            return ''

    @property
    def user_data(self):
        return User.objects.get(self.user)

    @property
    def id(self):
        return self.__id

    class Meta:
        connection = AW_SDB
        domain = 'comment'

    def save(self, **kwargs):
        self.__id = str(uuid.uuid4())
        super(Comment, self).save(**kwargs)

    def update(self, **kwargs):
        super(Comment, self).save(**kwargs)


class Match(models.Model):
    __id = models.ItemName()
    source = models.Field(required=True)
    target = models.Field(required=True)
    created_at = models.DateTimeField(default=datetime.now())
    status = models.Field(default='active')
    user = models.Field(required=True)
    type = models.Field(default='normal')

    @property
    def id(self):
        return self.__id

    class Meta:
        connection = AW_SDB
        domain = 'match'

    def save(self, **kwargs):
        self.__id = str(uuid.uuid4())
        logging.info('here tha!')
        super(Match, self).save(**kwargs)

    def update(self, **kwargs):
        super(Match, self).save(**kwargs)

    def save_points(self):

        if not Points.email_exists(self.user):
            logging.info('try to save points')

            points = Points()
            points.matching = 1
            points.user = self.user
            points.total = int(points.total) + 1

            try:
                points.save()
            except Exception as exc:
                logging.error(exc)
            else:
                logging.info('save point')

        else:

            try:
                points = Points.objects.get(self.user)
            except Exception as exc:
                logging.error(exc)
            else:
                points.matching = int(points.matching) + 1
                points.total = int(points.total) + 1

                try:
                    points.update()
                except Exception as exc:
                    logging.error(exc)
                else:
                    logging.info('update points')


class ApiUser(models.Model):
    email = models.ItemName()
    password = models.Field(default='')
    token = models.Field(default='')
    last_login_at = models.DateTimeField(default=datetime.now())
    #expiration_date = models.DateTimeField()
    expiration_date = models.Field()

    class Meta:
        connection = AW_SDB
        domain = 'api_user'

    @classmethod
    def email_exists(self, email):
        rs = self.objects.filter(
            simpledb.item_name(eq=email)
        )
        return True if len(rs) > 0 else False

    @classmethod
    def auth(self, email, password):
        password = str(hashlib.sha224(password).hexdigest())
        tot = len(self.objects.filter(
            simpledb.item_name(eq=email) & \
            simpledb.where(password=password)
        ))
        return True if tot > 0 else False

    def save(self, **kwargs):
        self.password = hashlib.sha224(self.password).hexdigest()
        super(ApiUser, self).save(**kwargs)

    def update(self, **kwargs):
        self.last_login_at = datetime.now()
        super(ApiUser, self).save(**kwargs)

    @classmethod
    def generate_token(self):
        while True:
            token = ('%s%s' % (
                settings.COOKIE_SECRET,
                uuid.uuid4().hex))[::-1]
            try:
                if self.objects.filter(token=token).count() == 0:
                    token = token
                    break
            except Exception as exc:
                logging.error(exc)
                break
        return token

    @classmethod
    def get_user_by_token(self, token):

        try:
            user = self.objects.filter(token=token)[0]
        except Exception as exc:
            logging.error(exc)
            user = None
        return user

    def has_expires(self):
        expiration_date = datetime.strptime(
            self.expiration_date, '%Y-%m-%dT%H:%M:%S')

        if not expiration_date > datetime.now():
            return True
        else:
            return False


class ApiProduct(models.Model):

    AUDIENCES_TYPES = (('all', 'all'),
                        ('local', 'local'),
                        ('private group', 'private group'))

    __id = models.ItemName()
    slug = models.Field()
    title = models.Field(default='')
    description = models.Field(default='')
    cost = models.NumberField(default=0)
    will_travel = models.NumberField(default=0)
    allow_retailers = models.BooleanField(default=False)
    allow_charities = models.BooleanField(default=False)
    donate_now = models.BooleanField(default=False)
    status = models.Field(required=True, default='inactive')
    created_at = models.DateTimeField(default=datetime.now())
    last_modified_at = models.DateTimeField()
    published_at = models.DateTimeField()
    expires_at = models.DateTimeField()
    type = models.Field()
    user = models.Field()
    categories = models.Field()
    image = models.Field()
    location = models.Field(default='')
    audience = models.Field()

    class Meta:
        connection = AW_SDB
        domain = 'api_product'

    @property
    def id(self):
        return self.__id

    @classmethod
    def _generate_slug(cls, title):
        _count = 2
        _slug = slugify(title)
        while len(cls.objects.filter(slug=_slug)) > 0:
            _slug = slugify(u'%s %s' % (title, _count))
            _count += 1
        return _slug

    def save(self, **kwargs):
        self.__id = str(uuid.uuid4())
        super(ApiProduct, self).save(**kwargs)

    def update(self, **kwargs):
        self.last_modified_at = datetime.now()
        super(ApiProduct, self).save(**kwargs)

    @property
    def data_categories(self):
        res = []
        if isinstance(self.categories, str):
            try:
                res = [Category.objects.get(self.categories)]
            except:
                pass
        else:
            try:
                res = [Category.objects.get(id) for id in self.categories]
            except:
                pass
        return res

    @property
    def expire_days(self):
        days = 0
        try:
            expires_at = datetime.strptime(
                self.expires_at, '%Y-%m-%d %H:%M:%S'
            )
            days = (expires_at.date() - datetime.now().date()).days
        except Exception as exc:
            logging.error(exc)
        return days

    @classmethod
    def set_slug(self, title):
        logging.info('self.slug: %s' % self.slug)
        if not self.slug or self.slug is None or self.slug is '':
            logging.info('into generate slug')
            self.slug = self._generate_slug(title)

    def upload_img(self, body):

        try:
            fp = StringIO(body)
            cn = S3Connection(
                settings.AWS_KEY, settings.AWS_SECRET
            )
            bucket = cn.get_bucket(settings.S3_BUCKET_NAME)

            key = Key(bucket)
            key.key = 'upload/api/user/%s' % self.id
            key.set_contents_from_file(
                fp, headers={'Content-Type': 'image/jpeg'}
            )

            key.set_acl('public-read')
            url = 'http://%s.%s/%s' % (
                bucket.name,
                settings.S3_IMAGES_URL,
                key.key
            )
        except Exception as exc:
            logging.error(exc)
            return None
        else:
            return url


class Points(models.Model):
    user = models.ItemName()
    matching = models.Field(default='0')
    charity = models.Field(default='0')
    products = models.Field(default='0')
    total = models.Field(default='0')

    class Meta:
        connection = AW_SDB
        domain = 'points'

    @classmethod
    def email_exists(self, email):
        rs = self.objects.filter(
            simpledb.item_name(eq=email)
        )
        return True if len(rs) > 0 else False

    def save(self, **kwargs):
        #self.__id = str(uuid.uuid4())
        super(Points, self).save(**kwargs)

    def update(self, **kwargs):
        #self.__id = str(uuid.uuid4())
        super(Points, self).save(**kwargs)
