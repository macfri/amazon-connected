import logging

from models import User
from tornado.web import RequestHandler

from libs.pagination import Paginator


class BaseHandler(RequestHandler):

    def __init__(self, *args, **kwargs):
        super(BaseHandler, self).__init__(*args, **kwargs)

    def get_current_user(self):
        _user = self.get_secure_cookie("user")
        if _user:
            try:
                _user = User.objects.get(_user, consistent_read=True)
            except Exception as exc:
                logging.error(exc)
            else:
                return _user
        return None

    def render_string(self, template, **kwargs):
        kwargs.update({'handler': self})
        return self.settings.get('template_env')\
            .get_template(template).render(**kwargs)

    def render(self, template, **kwargs):
        self.finish(self.render_string(template, **kwargs))


class ListMixin(object):

    @property
    def current_page(self):
        current_page = self.get_argument('page', '1')
        return int(current_page) if current_page.isdigit() else 1

    def get_pagination(self, query, count, per_page=10):

        try:
            per_page = int(per_page)
        except ValueError:
            per_page = 10

        page = self.current_page
        paginator = Paginator(page=self.current_page, total_items=count,
                              per_page=per_page)
        per_page = paginator.per_page

        return {
            'items': query[(page - 1) * per_page: page * per_page],
            'pages': paginator.pages,
            'total_items': count,
            'current_page': page,
            'total_pages': paginator.total_pages,
        }
