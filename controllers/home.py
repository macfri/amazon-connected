import simpledb
import logging

from models import Product, Category, User
from controllers import BaseHandler


class Index(BaseHandler):

    def get(self, slug=None):
        user = self.get_current_user()

        id_want = self.get_argument('id_want', None)
        id_have = self.get_argument('id_have', None)

        product_have = None
        product_want = None

        products_have = Product.objects.filter(type='have', status='active')
        products_want = Product.objects.filter(type='want', status='active')

        if slug:
            products_have = products_have.filter(categories__in=[slug])
            products_want = products_want.filter(categories__in=[slug])

        if id_want:
            product_want = Product.objects.get(id_want)

        if id_have:
            product_have = Product.objects.get(id_have)

        self.render('site/home.html',
            user=user,
            products_have=products_have,
            products_want=products_want,
            product_have=product_have,
            product_want=product_want,
            categories=Category.objects.all()
        )


class Search(BaseHandler):

    def get(self):

        allow_charities = self.get_argument('allow_charities', None)
        user = self.get_argument('user', None)
        type = self.get_argument('type', None)
        category = self.get_argument('category', None)

        products = Product.objects.filter(type=type, status='active')

        logging.info(len(products))

        if user:
            user = self.get_current_user()
            if hasattr(user, 'email'):
                logging.info('with user')
                logging.info('email: %s' % user.email)
                products = products.filter(user=user.email)
                logging.info(len(products))


        if allow_charities:
            products = products.filter(allow_charities=str(True))

        if category:
            products = products.filter(
                simpledb.where(categories__in=[category])
            )

        try:
            data = [dict(
                        username=x.data_user.name,
                        title=x.title,
                        image=x.image,
                        slug=x.slug,
                        description=x.description,
                        cost=x.cost,
                        will_travel=x.will_travel,
                        expires_at=x.expires_at,
                        categories=[y.title for y in  x.data_categories]
                    ) for x in products]
        except Exception as exc:
            logging.error(exc)
            data = []

        self.finish(dict(data=data))
