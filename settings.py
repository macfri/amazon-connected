import os

from jinja2 import Environment, FileSystemLoader

DEBUG = False
XSRF_COOKIES = False
COOKIE_SECRET = '2Ykh63389Hcxd2gbQ5c97n9u7u58R913'
LOGIN_URL = '/user/login'

DOMAIN_URL = ""

AWS_KEY = ''
AWS_SECRET = ''

S3_BUCKET_NAME = ''
S3_IMAGES_URL = ''

AWS_EMAIL_FROM = ''
AWS_UPLOAD_PATH = ''

FACEBOOK_API_KEY = ''
FACEBOOK_SECRET = ''

STATIC_PATH = os.path.join(os.path.dirname(__file__), "static")
STATIC_URL_PREFIX = "/static/"

TEMPLATE_PATH = os.path.join(os.path.dirname(__file__), 'templates')
TEMPLATE_ENV = Environment(loader=FileSystemLoader(TEMPLATE_PATH))

S3_UPLOAD_PATH_PRODUCT = ''
S3_UPLOAD_PATH_PRODUCT_API = ''

S3_UPLOAD_URL_PRODUCT = 'http://%s.%s/%s/' % (
    S3_BUCKET_NAME, S3_IMAGES_URL, S3_UPLOAD_PATH_PRODUCT)

try:
    from local_settings import *
except ImportError:
    pass
