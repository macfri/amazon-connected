#import Image
from PIL import Image
import sys
import re
_slugify_strip_re = re.compile(r'[^\w\s-]')
_slugify_hyphenate_re = re.compile(r'[-\s]+')


from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE


def slugify(value):
    import unicodedata
    if not isinstance(value, unicode):
        value = unicode(value)
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = unicode(_slugify_strip_re.sub('', value).strip().lower())
    return _slugify_hyphenate_re.sub('-', value)


def thumbnail(data, width, height,
    force=True, crop=True, adjust_to_width=True):
    """
    :force Si la imagen debe reducirse al tamano exacto proporcionado.

    :crop Si force es verdadero entonces indica que el thumb se creara a
    partir de centrar y cortar.

    :adjust_to_width Solo si force es False, si la imagen debe ajustarse al
    width proporcionado. Sirve en el caso que height sea mayor que el ancho
    y se necesita ajustar respecto a este ultimo. Usa el metodo thumbnail de
    PIL.

    src = '/tmp/foo.jpg'
    src = Image.open(src)
    for i, s in enumerate((
        (100, 285),
        (200, 100),)):
        x = thumbnail(src, *s, force=False)
        x.save('/tmp/%s.jpg' % i)
    """
    if isinstance(data, (str, unicode)):
        img = Image.open(data)
    else:
        img = data if force else data.copy()

    src_width, src_height = img.size

    if not force:
        if adjust_to_width and src_height > src_width:
            height = sys.maxint
        img.thumbnail((width, height), Image.ANTIALIAS)
    else:
        src_ratio = float(src_width) / float(src_height)
        dst_width, dst_height = width, height
        dst_ratio = float(dst_width) / float(dst_height)

        if dst_ratio < src_ratio:
            crop_height = src_height
            crop_width = crop_height * dst_ratio
            x_offset = float(src_width - crop_width) / 2
            y_offset = 0
        else:
            crop_width = src_width
            crop_height = crop_width / dst_ratio
            x_offset = 0
            y_offset = float(src_height - crop_height) / 3

        if crop:
            img = img.crop((
                int(x_offset), int(y_offset),
                int(x_offset) + int(crop_width),
                int(y_offset) + int(crop_height)
            ))
        else:
            dst_height = int(dst_width / src_ratio)

        img = img.resize((dst_width, dst_height), Image.ANTIALIAS)

    return img


def check_email(email):
    if len(email) > 6:
        if re.match('[\w\.-]+@[\w\.-]+\.\w{2,4}', email) != None:
            return True
    return False


class Mail(object):

    def __init__(self, subject='', sender='', to=None, cc=None, bcc=None):
        """
        @to list
        @cc list
        @bcc list
        """
        def to_list(val):
            if isinstance(val, list):
                return val
            else:
                return list(val) if val else []

        self._subject = subject
        self._sender = sender
        self._to = to_list(to)
        self._cc = to_list(cc)
        self._bcc = to_list(bcc)
        self._body = []

    @property
    def subject(self):
        return self._subject

    @subject.setter
    def subject(self, value=""):
        self._subject = value

    @property
    def sender(self):
        return self._sender

    @sender.setter
    def sender(self, value=""):
        self._sender = value

    @property
    def to(self):
        return self._to

    @to.setter
    def to(self, value=[]):
        value = value or []
        if isinstance(value, str):
            value = [value]
        self._to = list(value)

    @property
    def cc(self):
        return self._cc

    @cc.setter
    def cc(self, value):
        value = value or []
        if isinstance(value, str):
            value = [value]
        self._cc = list(value)

    @property
    def bcc(self):
        return self._bcc

    @bcc.setter
    def bcc(self, value):
        value = value or []
        if isinstance(value, str):
            value = [value]
        self._bcc = list(value)

    def add_body(self, content, type="plain"):
        self._body.append(MIMEText(content.encode("utf-8"), type, "utf-8"))

    def as_string(self):
        _mail = MIMEMultipart("alternative")
        _mail["Subject"] = self._subject
        _mail["From"] = self._sender
        _mail["To"] = COMMASPACE.join(self._to)
        if len(self._cc) > 0:
            _mail["CC"] = COMMASPACE.join(self._cc)
        for b in self._body:
            _mail.attach(b)
        return _mail.as_string()
