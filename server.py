import settings

import tornado
import tornado.ioloop

from tornado.httpserver import HTTPServer
from tornado.options import define, options
from tornado.web import Application

from urls import handlers


global_settings = dict((setting.lower(), getattr(settings, setting))
for setting in dir(settings) if setting.isupper())


def main():
    define('port', type=int, default=8888)
    define('host', type=str, default='127.0.0.1')

    application = Application(
        handlers=handlers, **global_settings
    )

    tornado.options.parse_command_line()
    http_server = HTTPServer(application, xheaders=True)
    http_server.listen(options.port, options.host)

    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()


if __name__ == '__main__':
    main()
