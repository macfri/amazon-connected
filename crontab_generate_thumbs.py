from boto.sqs.connection import SQSConnection
import settings
import logging

from models import Product

from boto.s3.connection import S3Connection
from boto.s3.key import Key
from cStringIO import StringIO

import Image as Image_

from utils import thumbnail


conn = SQSConnection(settings.AWS_KEY, settings.AWS_SECRET)
q = conn.create_queue('generate_thumbs')
messages = q.get_messages(10)
#visibility_timeout=60


print len(messages)

if len(messages) > 0:
    for m in messages:

        try:
            product = Product.objects.get(m.get_body())
        except Exception as exc:
            print exc
            logging.error(exc)
        else:

            print "product: %s" % product.id

            cn = S3Connection(
                settings.AWS_KEY, settings.AWS_SECRET
            )

            bucket = cn.get_bucket(settings.S3_BUCKET_NAME)
            key = Key(bucket)
            key.key = '%s/%s' % (settings.S3_UPLOAD_PATH_PRODUCT, product.id)

            try:
                image = Image_.open(StringIO(key.get_contents_as_string()))
            except Exception as exc:
                print exc
            else:

                try:
                    for width, height, suffix in  Product.THUMB_SIZES_IMAGES:
                        #print width, height
                        output_image = StringIO()
                        thumb = thumbnail(image, width, height, force=True,
                                crop=False, adjust_to_width=True)
                        thumb.save(output_image, 'jpeg')

                        key.key = '%s/%s_%s' % (
                            settings.S3_UPLOAD_PATH_PRODUCT,
                            product.id, suffix)

                        key.set_contents_from_string(
                            output_image.getvalue(),
                            headers={'Content-Type': 'image/jpeg'}
                        )
                        key.set_acl('public-read')
                except Exception as exc:
                    print exc
                    logging.error(exc)
                    #generate_thumbs.retry(exc=exc)
                else:
                    product.is_upload_thumbs = True

                    try:
                        product.update()
                    except Exception as exc:
                        logging.error()

                    q.delete_message(m)
